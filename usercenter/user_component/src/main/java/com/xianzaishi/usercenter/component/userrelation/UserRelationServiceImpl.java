package com.xianzaishi.usercenter.component.userrelation;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.xianzaishi.couponcenter.client.usercoupon.UserCouponService;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.usercenter.client.userachievement.query.UserAchievementQuery;
import com.xianzaishi.usercenter.client.userrelation.UserRelationService;
import com.xianzaishi.usercenter.client.userrelation.dto.UserRelationDTO;
import com.xianzaishi.usercenter.client.userrelation.query.UserRelationQuery;
import com.xianzaishi.usercenter.component.sendreward.RewardFactory;
import com.xianzaishi.usercenter.dal.userachievement.dao.UserAchievementDOMapper;
import com.xianzaishi.usercenter.dal.userachievement.dataobject.UserAchievementDO;
import com.xianzaishi.usercenter.dal.userrelation.dao.UserRelationDOMapper;
import com.xianzaishi.usercenter.dal.userrelation.dataobject.UserRelationDO;

@Service("userRelationService")
public class UserRelationServiceImpl implements UserRelationService {

  private static final Logger LOGGER = Logger.getLogger(UserRelationServiceImpl.class);

  @Autowired
  private UserRelationDOMapper userRelationDOMapper;

  @Autowired
  private UserAchievementDOMapper userAchievementDOMapper;

  @Autowired
  private UserCouponService userCouponService;

  @Autowired
  private RewardFactory rewardFactory;

  @Override
  public Result<List<UserRelationDTO>> queryUserRelationByUserId(Long userId) {
    if (null == userId) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "query parameter is error");
    }
    List<UserRelationDO> userRelationDOs = userRelationDOMapper.selectByUserId(userId);
    if (CollectionUtils.isEmpty(userRelationDOs)) {
      return Result.getErrDataResult(ServerResultCode.USER_UNEXIST,
          "user relation is not exist with this user id");
    }
    List<UserRelationDTO> userRelationDTOs = Lists.newArrayList();
    BeanCopierUtils.copyListBean(userRelationDOs, userRelationDTOs, UserRelationDTO.class);// bean拷贝
    return Result.getSuccDataResult(userRelationDTOs);
  }

  @Override
  public Result<Long> queryUserRelationCount(UserRelationQuery query) {
    Long queryCount = userRelationDOMapper.selectCountByQuery(query);
    LOGGER.info("query count:" + queryCount);
    return Result.getSuccDataResult(queryCount);
  }

  @Override
  public Result<Boolean> insertUserRelation(UserRelationDTO userRelationDTO) {
    if (null == userRelationDTO) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "query parameter is error");
    }
    UserRelationDO userRelationDO = new UserRelationDO();
    BeanCopierUtils.copyProperties(userRelationDTO, userRelationDO);
    int insertCount = userRelationDOMapper.insert(userRelationDO);
    if (insertCount < 1) {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_DATA_NOT_EXIST,
          "insert user relation error");
    }
    return Result.getSuccDataResult(true);
  }

  @Override
  public Result<Boolean> updateUserRelation(UserRelationDTO userRelationDTO) {
    if (null == userRelationDTO) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "parameter is error");
    }
    UserRelationQuery query = new UserRelationQuery();
    query.setRelatedId(userRelationDTO.getRelatedId());
    query.setType(userRelationDTO.getType());
    query.setStatus(userRelationDTO.getStatus());
    List<UserRelationDO> userRelationDOs = userRelationDOMapper.selectByQuery(query);
    if (CollectionUtils.isEmpty(userRelationDOs)) {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_DATA_NOT_EXIST,
          "user relation is not exist with this query");
    }
    UserRelationDO userRelationDO = userRelationDOs.get(0);
    if((userRelationDO.getRelationTag() & 0x1) != 0){
      return Result.getSuccDataResult(true);
    }
    userRelationDO.setRelationTag(userRelationDTO.getRelationTag());
//    BeanCopierUtils.copyProperties(userRelationDTO, userRelationDO);
    int updateCount = userRelationDOMapper.updateByPrimaryKey(userRelationDO);
    if (updateCount < 1) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "update user relation error");
    }
    // 用户关系确定之后更新用户成就
    if ((userRelationDO.getRelationTag() & 0x1) != 0) {
      userRelationDTO.setUserId(userRelationDO.getUserId());
      Result<Boolean> updateAchievementResult = updateUserAchievement(userRelationDTO);
      if (null == updateAchievementResult || !updateAchievementResult.getSuccess()
          || null == updateAchievementResult.getModule() || !updateAchievementResult.getModule()) {
        LOGGER.error("重新更新用户成就结果：" + JackSonUtil.getJson(updateUserAchievement(userRelationDTO)));
      }
    }
    return Result.getSuccDataResult(true);
  }

  @Override
  public Result<List<UserRelationDTO>> queryUserRelationByQuery(UserRelationQuery query) {
    if (null == query) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "query parameter is error");
    }
    List<UserRelationDO> userRelationDOs = userRelationDOMapper.selectByQuery(query);
    if (CollectionUtils.isEmpty(userRelationDOs)) {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_DATA_NOT_EXIST,
          "user relation is not exist with this query");
    }
    List<UserRelationDTO> userRelationDTOs = Lists.newArrayList();
    BeanCopierUtils.copyListBean(userRelationDOs, userRelationDTOs,UserRelationDTO.class);
    if (CollectionUtils.isNotEmpty(userRelationDTOs)) {
      return Result.getSuccDataResult(userRelationDTOs);
    }

    return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "bean copier error");
  }


  /**
   * 更新用户成就
   * 
   * @param userRelationDTO
   * @return
   */
  public Result<Boolean> updateUserAchievement(UserRelationDTO userRelationDTO) {
    if (null == userRelationDTO) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "parameter is error");
    }
    Long userId = userRelationDTO.getUserId();
    // 查询用户成就
    UserAchievementQuery query = new UserAchievementQuery();
    query.setUserId(userId);
    query.setEventId((long) 1);
    query.setType(userRelationDTO.getType());
    query.setStatus(1);
    List<UserAchievementDO> userAchievementDOs = userAchievementDOMapper.selectByQuery(query);
    Boolean result = false;
    if (CollectionUtils.isNotEmpty(userAchievementDOs)) {
      UserAchievementDO userAchievementDO = userAchievementDOs.get(0);
      userAchievementDO.setAchievementValue(userAchievementDO.getAchievementValue() + 1);
      int updateResult = userAchievementDOMapper.updateByPrimaryKey(userAchievementDO);
      result = updateResult > 0 ? true : false;
//      rewardFactory.sendExtraReward(userAchievementDO.getAchievementValue(), userId);
    } else {
      UserAchievementDO userAchievementDO = getUserAchievement(userRelationDTO);
      int insertResult = userAchievementDOMapper.insert(userAchievementDO);
      result = insertResult > 0 ? true : false;
//      rewardFactory.sendExtraReward(userAchievementDO.getAchievementValue(), userId);
    }
    rewardFactory.sendReward(userId, 7);
    
    return Result.getSuccDataResult(result);
  }

  /**
   * 获取用户成就信息
   * 
   * @param userRelationDTO
   * @return
   */
  private UserAchievementDO getUserAchievement(UserRelationDTO userRelationDTO) {
    UserAchievementDO userAchievementDO = new UserAchievementDO();
    userAchievementDO.setUserId(userRelationDTO.getUserId());
    userAchievementDO.setAchievementValue(1);
    userAchievementDO.setAchievementEvent("邀请");
    userAchievementDO.setEventId(1L);
    userAchievementDO.setStatus(1);
    userAchievementDO.setType(userRelationDTO.getType());
    return userAchievementDO;
  }

}
