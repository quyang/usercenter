package com.xianzaishi.usercenter.component.sendreward;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.xianzaishi.couponcenter.client.usercoupon.UserCouponService;
import com.xianzaishi.couponcenter.client.usercoupon.query.SendParaDTO;
import com.xianzaishi.couponcenter.client.usercoupon.query.SendParaDTO.SendTypeConstants;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;

@Component("sendCouponReward")
public class SendCouponReward extends SendRewardProduct {
  private static final Logger LOGGER = Logger.getLogger(SendCouponReward.class);
  
  @Autowired
  private UserCouponService userCouponService;

  @Override
  Result<Boolean> sendReward(Long userId, Integer packageId) {
    SendParaDTO send = new SendParaDTO();
    send.setPackageId(packageId);
    send.setSendType(SendTypeConstants.BY_COUPON_PACKAGE);
    send.setUserId(userId);
    Result<Boolean> sendCoupons = null;
    try {
      LOGGER.error("开始调用优惠券接口");
      sendCoupons = userCouponService.sendCoupon(send);// 给用户发放奖励
      if (null == sendCoupons || !sendCoupons.getSuccess() || null == sendCoupons.getModule()
          || !sendCoupons.getModule()) {
        // 如果发送优惠券失败，再次发送
        sendCoupons = userCouponService.sendCoupon(send);
        LOGGER.error("重新给" + userId + "发放奖励结果：" + JackSonUtil.getJson(sendCoupons));
      }
      LOGGER.error("调用优惠券接口结束");
    } catch (Exception e) {
      LOGGER.error("调用发送优惠券接口失败，原因：" + e.getMessage());
    }
    return sendCoupons;
  }

  @Override
  Result<Boolean> sendExtraReward(Long userId, Integer packageId) {
    SendParaDTO send = new SendParaDTO();
    send.setPackageId(packageId);
    send.setSendType(SendTypeConstants.BY_COUPON_PACKAGE);
    send.setUserId(userId);
    Result<Boolean> sendCoupons = userCouponService.sendCoupon(send);// 给用户发放奖励
    if (null == sendCoupons || !sendCoupons.getSuccess() || null == sendCoupons.getModule()
        || !sendCoupons.getModule()) {
      // 如果发送优惠券失败，再次发送
      sendCoupons = userCouponService.sendCoupon(send);
      LOGGER.error("重新发送额外奖励结果：" + JackSonUtil.getJson(sendCoupons));
    }
    return sendCoupons;
  }

}
