package com.xianzaishi.usercenter.component.userachievement;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.usercenter.client.userachievement.UserAchievementService;
import com.xianzaishi.usercenter.client.userachievement.dto.UserAchievementDTO;
import com.xianzaishi.usercenter.client.userachievement.query.UserAchievementQuery;
import com.xianzaishi.usercenter.dal.userachievement.dao.UserAchievementDOMapper;
import com.xianzaishi.usercenter.dal.userachievement.dataobject.UserAchievementDO;

@Service("userAchievementService")
public class UserAchievementServiceImpl implements UserAchievementService {
  
  private static final Logger LOGGER = Logger.getLogger(UserAchievementServiceImpl.class);
  
  @Autowired
  private UserAchievementDOMapper userAchievementDOMapper;

  @Override
  public Result<List<UserAchievementDTO>> queryUserAchievement(UserAchievementQuery query) {
    if(null == query){
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "query parameter is error");
    }
    
    List<UserAchievementDO> userAchievementDOs = userAchievementDOMapper.selectByQuery(query);
    if(CollectionUtils.isEmpty(userAchievementDOs)){
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_DATA_NOT_EXIST, "user achievement is not exist with this query");
    }
    List<UserAchievementDTO> userAchievementDTOs = Lists.newArrayList();
    BeanCopierUtils.copyListBean(userAchievementDOs, userAchievementDTOs, UserAchievementDTO.class);
    
    if(CollectionUtils.isEmpty(userAchievementDTOs)){
      LOGGER.error("拷贝错误，userAchievementDOs数据为：" + JackSonUtil.getJson(userAchievementDOs));
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "bean copier error");
    }
    return Result.getSuccDataResult(userAchievementDTOs);
  }

  @Override
  public Result<Boolean> updateUserAchievement(UserAchievementDTO userAchievementDTO) {
    // TODO Auto-generated method stub
    return null;
  }

}
