package com.xianzaishi.usercenter.component.sendreward;

import com.xianzaishi.itemcenter.common.result.Result;

/**
 * 发放不同的奖励
 * @author dongpo
 *
 */
public abstract class SendRewardProduct {
  /**
   * 发放固定奖励
   * @param userId
   * @param packageId
   * @return
   */
  abstract Result<Boolean> sendReward(Long userId,Integer packageId);
  
  /**
   * 发放额外奖励
   * @param userId
   * @param packageId
   * @return
   */
  abstract Result<Boolean> sendExtraReward(Long userId,Integer packageId);

}
