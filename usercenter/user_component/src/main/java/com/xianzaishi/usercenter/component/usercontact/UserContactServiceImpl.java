package com.xianzaishi.usercenter.component.usercontact;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.usercenter.client.usercontact.UserContactService;
import com.xianzaishi.usercenter.client.usercontact.dto.UserContactDTO;
import com.xianzaishi.usercenter.dal.usercontact.dao.UserContactDOMapper;
import com.xianzaishi.usercenter.dal.usercontact.dataobject.UserContactDO;

/**
 * 实现完整的用户联系人接口
 * @author zhancang
 */
@Service("userContactService")
public class UserContactServiceImpl implements UserContactService{
  
  @Autowired
  private UserContactDOMapper myUserContactDOMapper;

  @Override
  public Result<Long> insertContact(UserContactDTO userContactDTO) {
    UserContactDO userContactDO = new UserContactDO();
    BeanCopierUtils.copyProperties(userContactDTO, userContactDO);//获取userContactDO数据
    Long contactUserId = 0L;//默认返回联系用户id为0L
    if(userContactDO != null){
      Integer insertNum = myUserContactDOMapper.insert(userContactDO);//插入用户联系数据
      contactUserId = insertNum > 0 ? userContactDO.getContactUserId() : 0L;//获取联系人id
    }
    return Result.getSuccDataResult(contactUserId);
  }

  @Override
  public Result<Boolean> updateContact(UserContactDTO userContactDTO) {
    UserContactDO userContactDO = new UserContactDO();
    BeanCopierUtils.copyProperties(userContactDTO, userContactDO);//获取userContactDO数据
    Boolean isUpdate = false;//默认返回false,即更新失败
    if(userContactDO != null){
      Integer updateNum = myUserContactDOMapper.updateByPrimaryKeySelective(userContactDO);//更新数据
      isUpdate = updateNum > 0 ? true : false;//获取是否更新
    }
    return Result.getSuccDataResult(isUpdate);
  }

  @Override
  public Result<List<UserContactDTO>> getUserContactList(Long userId) {
    List<UserContactDTO> userContactDTOs = new ArrayList<>();
    BeanCopierUtils.copyProperties(myUserContactDOMapper.selectByUserId(userId), userContactDTOs);
    
    return Result.getSuccDataResult(userContactDTOs);
  }

  @Override
  public Result<Boolean> deleteUserContact(Long contactUserId) {
    Integer deleteNum = myUserContactDOMapper.deleteByPrimaryKey(contactUserId);
    Boolean deleteResult = deleteNum > 0 ? true : false;
    return Result.getSuccDataResult(deleteResult);
  }

}