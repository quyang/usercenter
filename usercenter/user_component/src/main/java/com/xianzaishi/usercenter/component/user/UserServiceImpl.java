package com.xianzaishi.usercenter.component.user;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.xianzaishi.itemcenter.common.CodeFactoryUtils;
import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.usercenter.client.user.UserService;
import com.xianzaishi.usercenter.client.user.dto.BaseUserDTO;
import com.xianzaishi.usercenter.client.user.dto.UserDTO;
import com.xianzaishi.usercenter.dal.user.dao.BaseUserDOMapper;
import com.xianzaishi.usercenter.dal.user.dao.UserProfileDOMapper;
import com.xianzaishi.usercenter.dal.user.dao.UserTokenDOMapper;
import com.xianzaishi.usercenter.dal.user.dataobject.BaseUserDO;
import com.xianzaishi.usercenter.dal.user.dataobject.UserProfileDO;
import com.xianzaishi.usercenter.dal.user.dataobject.UserTokenDO;

/**
 * 实现用户服务接口
 * 
 * @author zhancang
 */
@Service("userService")
public class UserServiceImpl implements UserService {

  private static final Logger logger = Logger.getLogger(UserServiceImpl.class);

  @Autowired
  private BaseUserDOMapper myBaseUserDOMapper;

  @Autowired
  private UserTokenDOMapper myUserTokenDOMapper;

  @Autowired
  private UserProfileDOMapper myUserProfileDOMapper;


  @Override
  public Result<? extends BaseUserDTO> queryUserByUserId(Long userId, Boolean includeAll) {
    logger.error("Query user id:" + userId + ",include:" + includeAll);

    BaseUserDTO baseUserDTO = new BaseUserDTO();// 定义返回结果的类型
    BeanCopierUtils.copyProperties(myBaseUserDOMapper.selectByPrimaryKey(userId), baseUserDTO);// 将从只持久层查询出来的DO数据拷贝到DTO数据类型中
    if (includeAll) {
      UserDTO userDTO = new UserDTO();
      BeanCopierUtils.copyProperties(myUserProfileDOMapper.selectByPrimaryKey(userId), userDTO);
      baseUserToUserDTO(baseUserDTO, userDTO);// baseUserDTO字段拷贝到userDTO中
      return Result.getSuccDataResult(userDTO);// 返回全部的属性
    }
    return Result.getSuccDataResult(baseUserDTO);
  }

  /**
   * baseUserDTO属性拷贝到userDTO中
   * 
   * @param baseUserDTO
   * @param userDTO
   */
  private void baseUserToUserDTO(BaseUserDTO baseUserDTO, UserDTO userDTO) {
    userDTO.setName(baseUserDTO.getName());
    userDTO.setPhone(baseUserDTO.getPhone());
    userDTO.setPwd(baseUserDTO.getPwd());
  }

  @Override
  public Result<BaseUserDTO> queryBaseUserByToken(String token) {
    logger.error("Query user token:" + token);
    UserTokenDO userTokenDO = myUserTokenDOMapper.selectByPrimaryKey(token);// 根据token查询UserTokenDO数据
    BaseUserDTO baseUserDTO = new BaseUserDTO();// 定义要返回的结果类型
    if (userTokenDO == null) {
      return Result.getErrDataResult(-7, "数据库token不存在,请登陆");
    }
    BaseUserDO baseUserDO = myBaseUserDOMapper.selectByPrimaryKey(userTokenDO.getUserId());
    BeanCopierUtils.copyProperties(baseUserDO, baseUserDTO);// 按照userId查询持久层的BaseUserDO类型，再将其字段拷贝到DTO数据类型
    return Result.getSuccDataResult(baseUserDTO);
  }

  @Override
  public Result<? extends BaseUserDTO> queryUserByPhone(Long phone, Boolean includeAll) {
    logger.error("Query user phone:" + phone + ",include:" + includeAll);
    BaseUserDTO baseUserDTO = new BaseUserDTO();
    BeanCopierUtils.copyProperties(myBaseUserDOMapper.selectByTelephone(phone), baseUserDTO);
    if (includeAll && baseUserDTO != null) {
      return queryUserByUserId(baseUserDTO.getUserId(), includeAll);// 返回UserProfileDO数据类型
    }
    return Result.getSuccDataResult(baseUserDTO);
  }

  @Override
  public Result<String> userLogin(Long phone, String hardwareCode) {
    if (null == phone) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "phone或pwd为空");
    }
    BaseUserDO baseUserDO = myBaseUserDOMapper.selectByTelephone(phone);// 根据手机号查询用户基本信息
    if (null != baseUserDO) {
      String token = CodeFactoryUtils.createToken(baseUserDO.getUserId());// MD5随机生成32位字符串
      Long userId = baseUserDO.getUserId();
      UserTokenDO userTokenDO = new UserTokenDO();
      userTokenDO.setToken(token);// 设置token
      userTokenDO.setUserId(userId);// 设置userId
      userTokenDO.setHardwareCode(hardwareCode);
      List<UserTokenDO> userTokenDOByUserId = myUserTokenDOMapper.selectByUserId(userId);
      try {
        if (CollectionUtils.isNotEmpty(userTokenDOByUserId)) {
          Integer deleteNum = myUserTokenDOMapper.deleteByUserId(userId);
          if (deleteNum < 1) {
            return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "删除数据错误");
          }
        }
      } catch (Exception e) {
        System.out.println(e);
      }
      UserTokenDO userTokenDOByToken = myUserTokenDOMapper.selectByPrimaryKey(token);
      if (null != userTokenDOByToken && StringUtils.isNotBlank(userTokenDOByToken.getToken())) {
        userTokenDO.setToken(CodeFactoryUtils.createToken(userId));
      }
      Integer insertNum = myUserTokenDOMapper.insertSelective(userTokenDO);// 插入UserTokenDO数据
      token = insertNum == 1 ? token : null;// 插入成功则返回token，否则返回空

      return Result.getSuccDataResult(token);// 登陆成功返回用户token
    } else {
      return Result.getErrDataResult(ServerResultCode.USER_UNEXIST, "用户不存在");// 用户不存在返回错误代码
    }
  }

  @Override
  public Result<String> sendUserPhoneCheckCode(Long phone, Integer sendType) {

    // String checkCode = "123456";//测试用
    String checkCode = CodeFactoryUtils.createCheckCode();// 生成6位的验证码
    return Result.getSuccDataResult(checkCode);
  }

  @Override
  public Result<Long> registe(UserDTO user) {
    UserProfileDO userProfileDO = new UserProfileDO();
    BaseUserDO baseUserDO = new BaseUserDO();
    BeanCopierUtils.copyProperties(user, baseUserDO);// userDTO信息字段拷贝到BaseUserDO中
    BeanCopierUtils.copyProperties(user, userProfileDO);// 用户DTO信息字段拷贝到DO数据类型中
    Long userId = 0L;// 当插入数据失败或者插入数据为空的时候返回userId=0L

    if (baseUserDO != null && userProfileDO != null) {
      Integer insertBaseNum = myBaseUserDOMapper.insert(baseUserDO);// 插入用户基本信息数据
      if (insertBaseNum > 0) {
        userProfileDO.setUserId(baseUserDO.getUserId());
        Integer insertProfileNumber = myUserProfileDOMapper.insert(userProfileDO);// 插入用户扩展数据
        userId = insertProfileNumber > 0 ? userProfileDO.getUserId() : 0L;
      }
    }

    return Result.getSuccDataResult(userId);// 返回用户id
  }

  @Override
  public Result<Boolean> updateUserBaseInfo(BaseUserDTO updateDTO) {
    BaseUserDO baseUserDO = new BaseUserDO();
    BeanCopierUtils.copyProperties(updateDTO, baseUserDO);
    Boolean isUpdate = false;// 当更新数据为空或者数据库更新失败的时候isUpdate为false
    if (baseUserDO != null) {
      Integer updateNum = myBaseUserDOMapper.updateByPrimaryKeySelective(baseUserDO);// 更新数据库
      isUpdate = updateNum > 0 ? true : false;
    }
    return Result.getSuccDataResult(isUpdate);// 返回更新状态
  }

  @Override
  public Result<Boolean> updateUserProfileInfo(UserDTO updateDTO) {
    UserProfileDO userProfileDO = new UserProfileDO();
    BeanCopierUtils.copyProperties(updateDTO, userProfileDO);

    logger.error("参数 2 UserServiceIml.java hardwarecode="+userProfileDO.getAppversion()+", equipmentNumber="+userProfileDO.getEquipmentNumber()+", msgId="+userProfileDO.getMsgId()+", userId="+userProfileDO.getUserId());

    Boolean isUpdate = false;// 当更新数据为空或者数据库更新失败的时候isUpdate为false
    if (userProfileDO != null) {
      Integer updateNum = myUserProfileDOMapper.updateByPrimaryKeySelective(userProfileDO);// 更新数据库
      isUpdate = updateNum > 0 ? true : false;
    }

    logger.error("参数 3 isUpdate="+isUpdate);

    return Result.getSuccDataResult(isUpdate);// 返回更新状态
  }

  @Override
  public Result<Boolean> deleteUserToken(String token) {

    Integer deleteNum = myUserTokenDOMapper.deleteByPrimaryKey(token);
    Boolean deleted = deleteNum == 1 ? true : false;
    return Result.getSuccDataResult(deleted);
  }

  @Override
  public Result<BaseUserDTO> queryBaseUserByInviteCode(String inviteCode) {
    if (null == inviteCode) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "query parameter is error");
    }
    UserProfileDO userProfileDO = myUserProfileDOMapper.selectByInviteCode(inviteCode);
    if (null == userProfileDO || userProfileDO.getUserId() == 0) {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_DATA_NOT_EXIST,
          "user is not exit with this query");
    }
    BaseUserDO baseUserDO = myBaseUserDOMapper.selectByPrimaryKey(userProfileDO.getUserId());
    if (null == baseUserDO || baseUserDO.getUserId() == 0) {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_DATA_NOT_EXIST,
          "base user is not exit with this query");
    }
    BaseUserDTO baseUserDTO = new BaseUserDTO();
    BeanCopierUtils.copyProperties(baseUserDO, baseUserDTO);
    return Result.getSuccDataResult(baseUserDTO);
  }

  @Override
  public Result<List<BaseUserDTO>> queryBaseUserByIds(List<Long> userIds) {
    if (CollectionUtils.isEmpty(userIds)) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "query parameter is error");
    }
    List<BaseUserDO> baseUserDOs = myBaseUserDOMapper.selectByIds(userIds);
    if (CollectionUtils.isEmpty(baseUserDOs)) {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_DATA_NOT_EXIST,
          "query user is not exist");
    }
    List<BaseUserDTO> baseUserDTOs = Lists.newArrayList();
    BeanCopierUtils.copyListBean(baseUserDOs, baseUserDTOs, BaseUserDTO.class);
    if (CollectionUtils.isEmpty(baseUserDTOs)) {
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "bean copier error");
    }

    return Result.getSuccDataResult(baseUserDTOs);
  }


}
