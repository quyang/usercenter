package com.xianzaishi.usercenter.component.sendreward;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.xianzaishi.itemcenter.common.result.Result;

/**
 * 奖励工厂
 * @author dongpo
 *
 */
@Component("rewardFactory")
public class RewardFactory {
  
  @Autowired
  private SendCouponReward sendCouponReward;

  /**
   * 发放固定奖励
   * @param userId
   * @param packageId
   * @return
   */
  public Result<Boolean> sendReward(Long userId,Integer packageId){
    
    return sendCouponReward.sendReward(userId, packageId);
  }
  
  /**
   * 发放额外奖励
   * @param achievementValue
   * @param userId
   * @return
   */
  public Result<Boolean> sendExtraReward(Integer achievementValue, Long userId){
    Integer sendPackageId = null;// 后台针对老带新，固定配置7为key的优惠券组
    switch (achievementValue) {
      case 5:
        sendPackageId = 13;
        break;
      case 10:
        sendPackageId = 14;
        break;
      case 25:
        sendPackageId = 15;
        break;
      case 50:
        sendPackageId = 16;
        break;
      default:
        sendPackageId = 17;
        break;
    }
    if(null == sendPackageId || sendPackageId == 0){
      return Result.getSuccDataResult(true);
    }
    return sendCouponReward.sendExtraReward(userId, sendPackageId);
  }
}
