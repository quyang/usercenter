package com.xianzaishi.usercenter.component;

import org.springframework.stereotype.Service;

import com.xianzaishi.usercenter.client.SayHello;

/**
 * demo implements
 * 
 * @author zhancang
 */
@Service("sayHello")
public class SayHelloImpl implements SayHello {


  public String sayHello(String name) {
    StringBuffer sb = new StringBuffer();
    sb.append("hello ");
    sb.append(name);
    return sb.toString();
  }
}
