package com.xianzaishi.usercenter.dal.usercontact.dao;

import java.util.List;

import com.xianzaishi.usercenter.dal.usercontact.dataobject.UserContactDO;

public interface UserContactDOMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table user_contact
     *
     * @mbggenerated
     */
    int deleteByPrimaryKey(Long contactUserId);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table user_contact
     *
     * @mbggenerated
     */
    int insert(UserContactDO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table user_contact
     *
     * @mbggenerated
     */
    int insertSelective(UserContactDO record);

    /**
     * 通过主键查询联系人信息
     * @param contactUserId
     * @return
     */
    UserContactDO selectByPrimaryKey(Long contactUserId);
    
    /**
     * 通过用户id查询联系人信息
     * @param userId
     * @return
     */
    List<UserContactDO> selectByUserId(Long userId);

    /**
     * 更新联系人用户数据
     * @param record
     * @return
     */
    int updateByPrimaryKeySelective(UserContactDO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table user_contact
     *
     * @mbggenerated
     */
    int updateByPrimaryKey(UserContactDO record);
}