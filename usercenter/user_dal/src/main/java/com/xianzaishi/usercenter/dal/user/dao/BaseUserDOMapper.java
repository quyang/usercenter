package com.xianzaishi.usercenter.dal.user.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xianzaishi.usercenter.dal.user.dataobject.BaseUserDO;

public interface BaseUserDOMapper {

    /**
     * 插入基本用户信息
     * @param baseUserDO
     * @return
     */
    int insert(@Param("baseUserDO") BaseUserDO baseUserDO);

    /**
     * 选择性插入
     * @param baseUserDO
     * @return
     */
    int insertSelective(@Param("baseUserDO") BaseUserDO baseUserDO);

    /**
     * 按照主键查询用户基本信息
     */
    BaseUserDO selectByPrimaryKey(Long userId);
    
    /**
     * 批量查询数据
     * @param userIds
     * @return
     */
    List<BaseUserDO> selectByIds(@Param("userIds") List<Long> userIds);
    
    /**
     * 根据手机号查询用户基本信息
     * @param phone
     * @return
     */
    BaseUserDO selectByTelephone(Long phone);

    /**
     * 选择性更新
     * @param baseUserDO
     * @return
     */
    int updateByPrimaryKeySelective(BaseUserDO baseUserDO);

    /**
     * 更新数据
     * @param baseUserDO
     * @return
     */
    int updateByPrimaryKey(BaseUserDO baseUserDO);
}