package com.xianzaishi.usercenter.dal.usercontact.dataobject;

import java.math.BigDecimal;
import java.util.Date;

public class UserContactDO {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user_contact.contact_user_id
     *
     * @mbggenerated
     */
    private Long contactUserId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user_contact.user_id
     *
     * @mbggenerated
     */
    private Long userId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user_contact.name
     *
     * @mbggenerated
     */
    private String name;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user_contact.phone
     *
     * @mbggenerated
     */
    private Long phone;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user_contact.type
     *
     * @mbggenerated
     */
    private Short type;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user_contact.area
     *
     * @mbggenerated
     */
    private String area;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user_contact.address
     *
     * @mbggenerated
     */
    private String address;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user_contact.postcode
     *
     * @mbggenerated
     */
    private String postcode;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user_contact.poi_x
     *
     * @mbggenerated
     */
    private BigDecimal poiX;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user_contact.poi_y
     *
     * @mbggenerated
     */
    private BigDecimal poiY;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user_contact.status
     *
     * @mbggenerated
     */
    private Short status;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user_contact.gmt_create
     *
     * @mbggenerated
     */
    private Date gmtCreate;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user_contact.gmt_modified
     *
     * @mbggenerated
     */
    private Date gmtModified;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user_contact.contact_user_id
     *
     * @return the value of user_contact.contact_user_id
     *
     * @mbggenerated
     */
    public Long getContactUserId() {
        return contactUserId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user_contact.contact_user_id
     *
     * @param contactUserId the value for user_contact.contact_user_id
     *
     * @mbggenerated
     */
    public void setContactUserId(Long contactUserId) {
        this.contactUserId = contactUserId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user_contact.user_id
     *
     * @return the value of user_contact.user_id
     *
     * @mbggenerated
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user_contact.user_id
     *
     * @param userId the value for user_contact.user_id
     *
     * @mbggenerated
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user_contact.name
     *
     * @return the value of user_contact.name
     *
     * @mbggenerated
     */
    public String getName() {
        return name;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user_contact.name
     *
     * @param name the value for user_contact.name
     *
     * @mbggenerated
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user_contact.phone
     *
     * @return the value of user_contact.phone
     *
     * @mbggenerated
     */
    public Long getPhone() {
        return phone;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user_contact.phone
     *
     * @param phone the value for user_contact.phone
     *
     * @mbggenerated
     */
    public void setPhone(Long phone) {
        this.phone = phone;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user_contact.type
     *
     * @return the value of user_contact.type
     *
     * @mbggenerated
     */
    public Short getType() {
        return type;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user_contact.type
     *
     * @param type the value for user_contact.type
     *
     * @mbggenerated
     */
    public void setType(Short type) {
        this.type = type;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user_contact.area
     *
     * @return the value of user_contact.area
     *
     * @mbggenerated
     */
    public String getArea() {
        return area;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user_contact.area
     *
     * @param area the value for user_contact.area
     *
     * @mbggenerated
     */
    public void setArea(String area) {
        this.area = area == null ? null : area.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user_contact.address
     *
     * @return the value of user_contact.address
     *
     * @mbggenerated
     */
    public String getAddress() {
        return address;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user_contact.address
     *
     * @param address the value for user_contact.address
     *
     * @mbggenerated
     */
    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user_contact.postcode
     *
     * @return the value of user_contact.postcode
     *
     * @mbggenerated
     */
    public String getPostcode() {
        return postcode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user_contact.postcode
     *
     * @param postcode the value for user_contact.postcode
     *
     * @mbggenerated
     */
    public void setPostcode(String postcode) {
        this.postcode = postcode == null ? null : postcode.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user_contact.poi_x
     *
     * @return the value of user_contact.poi_x
     *
     * @mbggenerated
     */
    public BigDecimal getPoiX() {
        return poiX;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user_contact.poi_x
     *
     * @param poiX the value for user_contact.poi_x
     *
     * @mbggenerated
     */
    public void setPoiX(BigDecimal poiX) {
        this.poiX = poiX;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user_contact.poi_y
     *
     * @return the value of user_contact.poi_y
     *
     * @mbggenerated
     */
    public BigDecimal getPoiY() {
        return poiY;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user_contact.poi_y
     *
     * @param poiY the value for user_contact.poi_y
     *
     * @mbggenerated
     */
    public void setPoiY(BigDecimal poiY) {
        this.poiY = poiY;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user_contact.status
     *
     * @return the value of user_contact.status
     *
     * @mbggenerated
     */
    public Short getStatus() {
        return status;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user_contact.status
     *
     * @param status the value for user_contact.status
     *
     * @mbggenerated
     */
    public void setStatus(Short status) {
        this.status = status;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user_contact.gmt_create
     *
     * @return the value of user_contact.gmt_create
     *
     * @mbggenerated
     */
    public Date getGmtCreate() {
        return gmtCreate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user_contact.gmt_create
     *
     * @param gmtCreate the value for user_contact.gmt_create
     *
     * @mbggenerated
     */
    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user_contact.gmt_modified
     *
     * @return the value of user_contact.gmt_modified
     *
     * @mbggenerated
     */
    public Date getGmtModified() {
        return gmtModified;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user_contact.gmt_modified
     *
     * @param gmtModified the value for user_contact.gmt_modified
     *
     * @mbggenerated
     */
    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }
}