package com.xianzaishi.itemcentrer;

import java.net.MalformedURLException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.caucho.hessian.client.HessianProxyFactory;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.usercenter.client.user.UserService;
import com.xianzaishi.usercenter.client.userrelation.UserRelationService;
import com.xianzaishi.usercenter.client.userrelation.dto.UserRelationDTO;

public class AppTest {

  public static void main(String[] args) throws MalformedURLException {
    /*
     * <servlet> <!-- 配置 HessianServlet，Servlet的名字随便配置，例如这里配置成ServiceServlet-->
     * <servlet-name>ServiceServlet</servlet-name>
     * <servlet-class>com.caucho.hessian.server.HessianServlet</servlet-class>
     * 
     * <!-- 配置接口的具体实现类 --> <init-param> <param-name>service-class</param-name>
     * <param-value>gacl.hessian.service.impl.ServiceImpl</param-value> </init-param> </servlet>
     * <!-- 映射 HessianServlet的访问URL地址--> <servlet-mapping>
     * <servlet-name>ServiceServlet</servlet-name> <url-pattern>/ServiceServlet</url-pattern>
     * </servlet-mapping>
     */
    // 在服务器端的web.xml文件中配置的HessianServlet映射的访问URL地址
    String url = "http://localhost/usercenter/hessian/userRelationService";
    HessianProxyFactory factory = new HessianProxyFactory();
    UserRelationService service = (UserRelationService) factory.create(UserRelationService.class, url);// 创建IService接口的实例对象
//    // String i = service.sayHello("111111");
//    // System.out.println(i);
////    Long userId = 1L;
//    UserDTO user = new UserDTO();
//    user = getUserDTO(1244444L, "122345");
//    Result<Long> result = service.registe(user);
//    String jsonString = JackSonUtil.getJson(result);
//    System.out.println(jsonString);
//    service.queryUserByUserId(42L, true);
//    for(int i = 0;i < 10000;i++ ){
//      String token = createToken((long) i);
//      System.out.println(i);
//      if(token.length() != 32){
//        System.out.println(token);
//      }
//    }
    UserRelationDTO userRelationDTO = new UserRelationDTO();
    userRelationDTO.setRelatedId(44L);
    userRelationDTO.setType(1);
    userRelationDTO.setStatus((short) 1);
    userRelationDTO.setRelationTag(1);
    Result<Boolean> result = service.updateUserRelation(userRelationDTO);
    System.out.println(JackSonUtil.getJson(result));
  }
  
  private static final char HEX_DIGITS[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a',
    'b', 'c', 'd', 'e', 'f'};
private static final char[] code = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
public static String[] inviteChars = new String[] { "a", "b", "c", "d", "e", "f",
  "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s",
  "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5",
  "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I",
  "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
  "W", "X", "Y", "Z" };

public static String createToken(Long userId){
  String key = String.valueOf(System.currentTimeMillis()) + String.valueOf(userId);
  MessageDigest digest = null;
  try {
    digest = java.security.MessageDigest.getInstance("MD5");
  } catch (NoSuchAlgorithmException e) {
    e.printStackTrace();
  }
  digest.update(key.getBytes());
  byte messageDigest[] = digest.digest();
  return toTokenString(messageDigest,userId);
}

/**
 * 转换成32位字符串
 * @param b
 * @param userId 
 * @return
 */
private static String toTokenString(byte[] b, Long userId) {
  StringBuilder sb = new StringBuilder(b.length * 2);
  String userIdString = String.valueOf(userId);
  for (int i = 0; i < b.length; i++) {
    sb.append(HEX_DIGITS[(b[i] & 0xf0) >>> 4]);//byte转int为了不丢失符号位， 所以&0xFF,并且无符号右移4位
    sb.append(HEX_DIGITS[b[i] & 0x0f]);
  }
  sb.append(userIdString);
  String token = sb.substring(userIdString.length(),sb.length());
  return token;
}
  
//  /**
//   * 获取用户信息数据userDTO
//   * 
//   * @param registVO
//   */
//  private static UserDTO getUserDTO(Long phone, String checkCode) {
//    UserDTO userDTO = new UserDTO();
//    userDTO.setPhone(phone);
//    userDTO.setPwd("123");
//    userDTO.setUserType((short) 0);
//    userDTO.setStatus(UserStatusConstants.USER_STATUS_REGISTERING);
//    userDTO.setAge((short) 25);
//    userDTO.setName(String.valueOf(phone));
//    userDTO.setSex((short) 1);
//    userDTO.setUserTag(1);
//    userDTO.setActiveCode(getActiveCode(checkCode));// 设置用户激活码
//    Random random = new Random();
//    userDTO.setPic("1.png");
//    return userDTO;
//  }
//  
//  /**
//   * 获取用户激活码
//   * 
//   * @param checkCode
//   * @return
//   */
//  private static String getActiveCode(String checkCode) {
//    StringBuffer activeCode = new StringBuffer();
//    activeCode.append(checkCode);
//    activeCode.append(";");// 以;为分隔符
//    long date = new Date().getTime();// 当前发送验证码时间
//    activeCode.append(date);
//    return activeCode.toString();
//  }
}
