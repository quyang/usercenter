package com.xianzaishi.usercentrer;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;

@Controller
@RequestMapping(value = "/test")
public class UserController {

  @Autowired  
  private  HttpServletRequest request; 
  
  @RequestMapping(value = "/user/*")
  @ResponseBody
  public String requestByItemIds(String query) {
    //String url = request.getRequestURI();
    String jsonResult = JSON.toJSONString("TestResult");
    System.out.println(jsonResult);

    return jsonResult;
  }

}
