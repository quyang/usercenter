package com.xianzaishi.usercenter.client.userachievement;

import java.util.List;

import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.usercenter.client.userachievement.dto.UserAchievementDTO;
import com.xianzaishi.usercenter.client.userachievement.query.UserAchievementQuery;

public interface UserAchievementService {
  
  /**
   * 根据查询条件查询用户成就
   * @param query
   * @return
   */
  Result<List<UserAchievementDTO>> queryUserAchievement(UserAchievementQuery query);
  
  /**
   * 更新用户成就
   * @param userAchievementDTO
   * @return
   */
  Result<Boolean> updateUserAchievement(UserAchievementDTO userAchievementDTO);

}
