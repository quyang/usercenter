package com.xianzaishi.usercenter.client.userachievement.query;

import java.io.Serializable;

public class UserAchievementQuery implements Serializable {
  /**
   * serial version UID
   */
  private static final long serialVersionUID = 9204291114318898776L;

  /**
   * 用户id
   */
  private Long userId;
  
  /**
   * 事件id
   */
  private Long eventId;
  
  /**
   * 成就类型
   */
  private Integer type;

  /**
   * 成就状态
   */
  private Integer status;

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public Long getEventId() {
    return eventId;
  }

  public void setEventId(Long eventId) {
    this.eventId = eventId;
  }

  public Integer getType() {
    return type;
  }

  public void setType(Integer type) {
    this.type = type;
  }

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }
  
}
