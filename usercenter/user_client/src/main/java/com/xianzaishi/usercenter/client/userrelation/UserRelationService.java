package com.xianzaishi.usercenter.client.userrelation;

import java.util.List;

import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.usercenter.client.userrelation.dto.UserRelationDTO;
import com.xianzaishi.usercenter.client.userrelation.query.UserRelationQuery;

public interface UserRelationService {
  
  /**
   * 根据用户id查询用户关系
   * @param userId 用户id
   * @return
   */
  Result<List<UserRelationDTO>> queryUserRelationByUserId(Long userId);
  
  /**
   * 根据查询条件查询用户关系
   * @param query
   * @return
   */
  Result<List<UserRelationDTO>> queryUserRelationByQuery(UserRelationQuery query);
  
  /**
   * 查询用户关系数
   * @param query 查询条件
   * @return
   */
  Result<Long> queryUserRelationCount(UserRelationQuery query);
  
  /**
   * 建立用户关系
   * @param userRelationDTO
   * @return
   */
  Result<Boolean> insertUserRelation(UserRelationDTO userRelationDTO);
  
  /**
   * 更新用户关系
   * @param userRelationDTO
   * @return
   */
  Result<Boolean> updateUserRelation(UserRelationDTO userRelationDTO);
  
}
 