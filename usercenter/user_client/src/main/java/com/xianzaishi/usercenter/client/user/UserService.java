package com.xianzaishi.usercenter.client.user;

import java.util.List;

import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.usercenter.client.user.dto.BaseUserDTO;
import com.xianzaishi.usercenter.client.user.dto.UserDTO;

public interface UserService {

  /**
   * 查询用户基本信息
   * @param userId
   * @return 基础用户对象
   */
  public Result<? extends BaseUserDTO> queryUserByUserId(Long userId,Boolean includeAll);
  
  /**
   * 查询用户基本信息
   * @param userId
   * @return 基础用户对象
   */
  public Result<BaseUserDTO> queryBaseUserByToken(String token);
  
  /**
   * 根据邀请码查询用户基本信息
   * @param inviteCode
   * @return
   */
  public Result<BaseUserDTO> queryBaseUserByInviteCode(String inviteCode);
  
  /**
   * 通过手机号查询用户信息
   * @param phone
   * @return
   */
  public Result<? extends BaseUserDTO> queryUserByPhone(Long phone,Boolean includeAll);
  
  /**
   * 用户重新登录
   * @param phone
   * @param pwd
   * @return
   */
  public Result<String> userLogin(Long phone, String hardwareCode);
  
  /**
   * 给用户发送验证码
   * @param userId
   * @param sendType 1:初始注册发送验证码；2：忘记密码发送验证码
   * @return 发送成功时，只包含发送出去的验证码。发送失败时，包括发送失败原因和失败code
   */
  public Result<String> sendUserPhoneCheckCode(Long phone, Integer sendType);
  
  /**
   * 注册一个普通用户
   * @param user
   * @return 用户id
   */
  public Result<Long> registe(UserDTO user);
  
  /**
   * 更新用户信息、更新用户密码
   * @param updateDTO
   * @return
   */
  public Result<Boolean> updateUserBaseInfo(BaseUserDTO updateDTO);
  
  /**
   * 更新用户描述信息
   * @param updateDTO
   * @return
   */
  public Result<Boolean> updateUserProfileInfo(UserDTO updateDTO);
  
  /**
   * 用户退出删除token信息
   * @param token
   * @return
   */
  public Result<Boolean> deleteUserToken(String token);
  
  /**
   * 根据用户id组查询用户基本信息
   * @param userIds
   * @return
   */
  public Result<List<BaseUserDTO>> queryBaseUserByIds(List<Long> userIds);
}