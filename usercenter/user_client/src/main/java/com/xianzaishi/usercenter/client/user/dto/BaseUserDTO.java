package com.xianzaishi.usercenter.client.user.dto;

import java.io.Serializable;

import com.google.common.base.Preconditions;

/**
 * 对外暴露用户基本信息
 * 
 * @author zhancang
 */
public class BaseUserDTO implements Serializable {

  private static final long serialVersionUID = -7968624251816384211L;

  /**
   * 用户id
   */
  Long userId;

  /**
   * 用户手机号码
   */
  Long phone;

  /**
   * 用户密码
   */
  String pwd;

  /**
   * 用户名称
   */
  String name;

  /**
   * 用户状态
   */
  Short status = UserStatusConstants.USER_STATUS_REGISTERING;

  public Long getUserId() {
    return userId == null ? 0 : userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public Long getPhone() {
    return phone;
  }

  public void setPhone(Long phone) {
    this.phone = phone;
  }

  /**
   * 查询接口对外隐藏该数据项
   * 
   * @return
   */
  public String getPwd() {
    return pwd;
  }

  public void setPwd(String pwd) {
    this.pwd = pwd == null ? null : pwd.trim();
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name == null ? null : name.trim();
  }

  public Short getStatus() {
    return status;
  }

  public void setStatus(Short status) {
    Preconditions.checkNotNull(status, "status == null");
    Preconditions.checkArgument((UserStatusConstants.isCorrectParameter(status)), "status not in (0,1,4)");
    this.status = status;
  }

  /**
   * 用户状态
   * 
   * @author zhancang
   */
  public static class UserStatusConstants implements Serializable {

    private static final long serialVersionUID = -3467189058053114473L;

    /**
     * 用户正在注册中（刚刚发送了手机验证码）
     */
    public static final Short USER_STATUS_REGISTERING = 0;

    /**
     * 注册完成正常的用户
     */
    public static final Short USER_STATUS_REGISTRATION_COMPLETE = 1;

    /**
     * 清退用户
     */
    public static final Short USER_STATUS_REJECTED = 4;

    /**
     * 是否正确的参数
     * 
     * @param parameter
     * @return
     */
    public static boolean isCorrectParameter(Short parameter) {
      return parameter == USER_STATUS_REGISTERING || parameter == USER_STATUS_REGISTRATION_COMPLETE
          || parameter == USER_STATUS_REJECTED;
    }
  }
}
