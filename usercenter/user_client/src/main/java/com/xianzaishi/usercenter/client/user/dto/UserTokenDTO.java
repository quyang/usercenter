package com.xianzaishi.usercenter.client.user.dto;

import java.io.Serializable;
import java.util.Date;

import com.google.common.base.Preconditions;

/**
 * 用户登录信息
 * @author zhancang
 *
 */
public class UserTokenDTO implements Serializable{

  private static final long serialVersionUID = 4236100486383487472L;

  private Date now = new Date();
  
  /**
   * 登录token
   */
  private String token;

  /**
   * 用户id
   */
  private Long userId;
  
  /**
   * 创建时间
   */
  private Date  gmtCreate = now;

  /**
   * 修改时间
   */
  private Date  modifiedTime = now;

  /**
   * 设备号
   */
  private String hardwareCode;

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token == null ? null : token.trim();
  }

  public Long getUserId() {
    return userId == null ? 0 : userId;
  }

  public void setUserId(Long userId) {
    Preconditions.checkNotNull(userId, "userId == null");
    Preconditions.checkArgument(userId > 0, "userId <= 0");
    this.userId = userId;
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  public Date getModifiedTime() {
    return modifiedTime;
  }

  public void setModifiedTime(Date modifiedTime) {
    this.modifiedTime = modifiedTime;
  }

  public String getHardwareCode() {
    return hardwareCode;
  }

  public void setHardwareCode(String hardwareCode) {
    this.hardwareCode = hardwareCode == null ? null : hardwareCode.trim();
  }
}
