package com.xianzaishi.usercenter.client.userachievement.dto;

import java.io.Serializable;
import java.util.Date;

public class UserAchievementDTO implements Serializable{
  
  /**
   * serial version UID
   */
  private static final long serialVersionUID = -8807797927432439325L;

  /**
   * 成就id
   */
  private Long id;

  /**
   * 用户id
   */
  private Long userId;

  /**
   * 成就值
   */
  private Integer achievementValue;

  /**
   * 获得此成就的事件
   */
  private String achievementEvent;

  /**
   * 事件id
   */
  private Long eventId;

  /**
   * 创建时间
   */
  private Date gmtCreate;

  /**
   * 修改时间
   */
  private Date gmtModified;

  /**
   * 成就类型
   */
  private Integer type;

  /**
   * 成就状态
   */
  private Integer status;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public Integer getAchievementValue() {
    return achievementValue;
  }

  public void setAchievementValue(Integer achievementValue) {
    this.achievementValue = achievementValue;
  }

  public String getAchievementEvent() {
    return achievementEvent;
  }

  public void setAchievementEvent(String achievementEvent) {
    this.achievementEvent = achievementEvent;
  }

  public Long getEventId() {
    return eventId;
  }

  public void setEventId(Long eventId) {
    this.eventId = eventId;
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  public Date getGmtModified() {
    return gmtModified;
  }

  public void setGmtModified(Date gmtModified) {
    this.gmtModified = gmtModified;
  }

  public Integer getType() {
    return type;
  }

  public void setType(Integer type) {
    this.type = type;
  }

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }
  
}
