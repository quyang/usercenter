package com.xianzaishi.usercenter.client.userrelation.dto;

import java.io.Serializable;
import java.util.Date;

public class UserRelationDTO implements Serializable {
  
  /**
   * serial version UID
   */
  private static final long serialVersionUID = 5206199285463298009L;

  private Date now = new Date();
  
  /**
   * 用户关系id
   */
  private Long id;

  /**
   * A用户id
   */
  private Long userId;

  /**
   * B用户id
   */
  private Long relatedId;

  /**
   * 用户关系
   */
  private String relationship;

  /**
   * 用户关系类型
   */
  private Integer type;

  /**
   * 用户关系状态
   */
  private Short status;

  /**
   * 创建时间
   */
  private Date gmtCreate = now;

  /**
   * 修改时间
   */
  private Date gmtModified = now;

  /**
   * 邀请渠道
   */
  private Integer channel;

  /**
   * 关系标
   */
  private Integer relationTag;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public Long getRelatedId() {
    return relatedId;
  }

  public void setRelatedId(Long relatedId) {
    this.relatedId = relatedId;
  }

  public String getRelationship() {
    return relationship;
  }

  public void setRelationship(String relationship) {
    this.relationship = relationship;
  }

  public Integer getType() {
    return type;
  }

  public void setType(Integer type) {
    this.type = type;
  }

  public Short getStatus() {
    return status;
  }

  public void setStatus(Short status) {
    this.status = status;
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  public Date getGmtModified() {
    return gmtModified;
  }

  public void setGmtModified(Date gmtModified) {
    this.gmtModified = gmtModified;
  }

  public Integer getChannel() {
    return channel;
  }

  public void setChannel(Integer channel) {
    this.channel = channel;
  }

  public Integer getRelationTag() {
    return relationTag;
  }

  public void setRelationTag(Integer relationTag) {
    this.relationTag = relationTag;
  }
  
}
