package com.xianzaishi.usercenter.client.user.dto;

import java.io.Serializable;
import java.util.Date;

import com.google.common.base.Preconditions;

/**
 * 对外暴露用户全量信息
 * 
 * @author zhancang
 */
public class UserDTO extends BaseUserDTO implements Serializable {

  private static final long serialVersionUID = 6348496709378067461L;

  private Date now = new Date();
  /**
   * 用户图片
   */
  private String pic;

  /**
   * 用户类型
   */
  private Short userType = UserTypeConstants.COMMON;

  /**
   * 用户生日
   */
  private Date birthday;

  /**
   * 用户性别
   */
  private Short sex = UserSexConstants.SECRECY;

  /**
   * 用户邮件地址
   */
  private String mail;

  /**
   * 用户区域属性 TODO 看看插入端的国标码是否可以精确化
   */
  private String area;

  /**
   * 用户详细地址
   */
  private String address;

  /**
   * 用户签名
   */
  private String signature;

  /**
   * 用户创建时间
   */
  private Date gmtCreate = now;

  /**
   * 用户修改时间
   */
  private Date gmtModified = now;

  /**
   * 用户激活码
   */
  private String activeCode;

  /**
   * 用户各种标签
   */
  private Integer userTag;
  
  /**
   * 用户邀请码
   */
  private String inviteCode;

  /**
   * app版本
   */
  private String appversion;

  /**
   * 设备号
   */
  private String equipmentNumber;


  /**
   * 消息id
   */
  private String msgId;

  public String getMsgId() {
    return msgId;
  }

  public void setMsgId(String msgId) {
    this.msgId = msgId;
  }

  public String getAppversion() {
    return appversion;
  }

  public void setAppversion(String appversion) {
    this.appversion = appversion;
  }

  public String getEquipmentNumber() {
    return equipmentNumber;
  }

  public void setEquipmentNumber(String equipmentNumber) {
    this.equipmentNumber = equipmentNumber;
  }

  public String getPic() {
    return pic;
  }

  public void setPic(String pic) {
    this.pic = pic == null ? null : pic.trim();
  }

  public Short getUserType() {
    return userType;
  }

  public void setUserType(Short userType) {
    this.userType = userType;
  }

  public Date getBirthday() {
    return birthday;
  }

  public void setBirthday(Date birthday) {
    this.birthday = birthday;
  }

  public Short getSex() {
    return sex;
  }

  public void setSex(Short sex) {
    if(null != sex){
      Preconditions.checkArgument(UserSexConstants.isCorrectParameter(sex), "sex not in (0,1,2)");
    }
    this.sex = sex;
  }

  public String getMail() {
    return mail;
  }

  public void setMail(String mail) {
    this.mail = mail == null ? null : mail.trim();
  }

  public String getArea() {
    return area;
  }

  public void setArea(String area) {
    this.area = area == null ? null : area.trim();
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address == null ? null : address.trim();
  }

  public String getSignature() {
    return signature;
  }

  public void setSignature(String signature) {
    this.signature = signature == null ? null : signature.trim();
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  public Date getGmtModified() {
    return gmtModified;
  }

  public void setGmtModified(Date gmtModified) {
    this.gmtModified = gmtModified;
  }

  public String getActiveCode() {
    return activeCode;
  }

  public void setActiveCode(String activeCode) {
    this.activeCode = activeCode == null ? null : activeCode.trim();
  }

  public Integer getUserTag() {
    return userTag;
  }

  public void setUserTag(Integer userTag) {
    this.userTag = userTag;
  }
  
  public String getInviteCode() {
    return inviteCode;
  }

  public void setInviteCode(String inviteCode) {
    this.inviteCode = inviteCode;
  }




  /**
   * 定义用户类型常量
   * @author zhancang
   */
  public static class UserTypeConstants implements Serializable {

    private static final long serialVersionUID = 244467853823990837L;

    /**
     * 普通用户
     */
    public static final Short COMMON = 1;
    
    /**
     * 是否正确的参数
     * 
     * @param parameter
     * @return
     */
    public static boolean isCorrectParameter(Short parameter) {
      return parameter == COMMON;
    }

  }

  public static class UserSexConstants implements Serializable {

    private static final long serialVersionUID = -5145942960291691787L;

    /**
     * 男
     */
    public static final Short MALE = 1;

    /**
     * 女
     */
    public static final Short FEMALE = 0;
    
    /**
     * 保密状态
     */
    public static final Short SECRECY = 2;
    
    /**
     * 是否正确的参数
     * 
     * @param parameter
     * @return
     */
    public static boolean isCorrectParameter(Short parameter) {
      return parameter == MALE || parameter == FEMALE || parameter == SECRECY;
    }
  }
}
