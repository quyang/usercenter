package com.xianzaishi.usercenter.client.usercontact;

import java.util.List;

import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.usercenter.client.usercontact.dto.UserContactDTO;

public interface UserContactService {

  /**
   * 创建一个联系人详细
   * @param userContactDTO
   * @return
   */
  public Result<Long> insertContact(UserContactDTO userContactDTO);
  
  /**
   * 更新一个已有的联系人详细信息
   * @param userContactDTO
   * @return
   */
  public Result<Boolean> updateContact(UserContactDTO userContactDTO);
  
  /**
   * 查询指定用户的联系人列表，暂时不考虑分页
   * @param userId
   * @return
   */
  public Result<List<UserContactDTO>> getUserContactList(Long userId);
  
  /**
   * 删除联系人
   * @param contactUserId
   * @return
   */
  public Result<Boolean> deleteUserContact(Long contactUserId);
  
}