package com.xianzaishi.usercenter.client.userrelation.query;

import java.io.Serializable;

/**
 * 用户关系查询条件
 * @author dongpo
 *
 */
public class UserRelationQuery implements Serializable {
  
  /**
   * serial version UID
   */
  private static final long serialVersionUID = 3706394365366080902L;

  /**
   * 用户关系id
   */
  private Long id;
  
  /**
   * A用户id
   */
  private Long userId;
  
  /**
   * B用户id
   */
  private Long relatedId;
  
  /**
   * 关系类型
   */
  private Integer type;
  
  /**
   * 关系状态
   */
  private Short status;
  
  /**
   * 建立关系的渠道
   */
  private Integer channel;
  
  /**
   * 用户关系标
   */
  private Integer relationTag;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public Long getRelatedId() {
    return relatedId;
  }

  public void setRelatedId(Long relatedId) {
    this.relatedId = relatedId;
  }

  public Integer getType() {
    return type;
  }

  public void setType(Integer type) {
    this.type = type;
  }

  public Short getStatus() {
    return status;
  }

  public void setStatus(Short status) {
    this.status = status;
  }

  public Integer getChannel() {
    return channel;
  }

  public void setChannel(Integer channel) {
    this.channel = channel;
  }

  public Integer getRelationTag() {
    return relationTag;
  }

  public void setRelationTag(Integer relationTag) {
    this.relationTag = relationTag;
  }

}
