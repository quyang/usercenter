package com.xianzaishi.usercenter.client.usercontact.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.google.common.base.Preconditions;

/**
 * 用户联系人对象
 * 
 * @author zhancang
 */
public class UserContactDTO implements Serializable {

  private static final long serialVersionUID = -2883710930944888961L;

  /**
   * 联系人id
   */
  private Long contactUserId;

  /**
   * 用户id
   */
  private Long userId;

  /**
   * 联系人名称
   */
  private String name;

  /**
   * 联系人手机
   */
  private Long phone;

  /**
   * 是否默认联系人
   */
  private Boolean defaultContact = true;

  /**
   * 区域地址 TODO 看看插入端的国标码是否可以精确化
   */
  private String area;

  /**
   * 联系人详细地址
   */
  private String address;

  /**
   * 联系人邮编
   */
  private String postcode;

  /**
   * 联系人坐标点-X
   */
  private BigDecimal poiX;

  /**
   * 联系人坐标点-Y
   */
  private BigDecimal poiY;

  /**
   * 联系人状态
   */
  private Short status = CONTACT_STATUS.CONTACT_STATUS_VALID;

  /**
   * 创建时间
   */
  private Date gmtCreate;

  /**
   * 修改时间
   */
  private Date gmtModified;

  public Long getContactUserId() {
    return contactUserId == null ? 0 : contactUserId;
  }

  public void setContactUserId(Long contactUserId) {
    Preconditions.checkNotNull(contactUserId, "contactUserId == null");
    Preconditions.checkArgument(contactUserId > 0, "contactUserId <= 0");
    this.contactUserId = contactUserId;
  }

  public Long getUserId() {
    return userId == null ? 0 : userId;
  }

  public void setUserId(Long userId) {
    Preconditions.checkNotNull(userId, "userId != null");
    Preconditions.checkArgument(userId > 0, "userId <= 0");
    this.userId = userId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name == null ? null : name.trim();
  }

  public Long getPhone() {
    return phone == null ? 0 : phone;
  }

  public void setPhone(Long phone) {
    Preconditions.checkNotNull(phone, "phone != null");
    Preconditions.checkArgument(phone > 0, "phone <= 0");
    this.phone = phone;
  }

  public Boolean getDefaultContact() {
    return defaultContact;
  }

  public void setDefaultContact(Boolean defaultContact) {
    this.defaultContact = defaultContact;
  }

  public boolean isDefaultContact() {
    return defaultContact;
  }

  public String getArea() {
    return area;
  }

  public void setArea(String area) {
    this.area = area == null ? null : area.trim();
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address == null ? null : address.trim();
  }

  public String getPostcode() {
    return postcode;
  }

  public void setPostcode(String postcode) {
    this.postcode = postcode == null ? null : postcode.trim();
  }

  public BigDecimal getPoiX() {
    return poiX == null ? new BigDecimal(0) : poiX;
  }

  public void setPoiX(BigDecimal poiX) {
    this.poiX = poiX;
  }

  public BigDecimal getPoiY() {
    return poiY == null ? new BigDecimal(0) : poiY;
  }

  public void setPoiY(BigDecimal poiY) {
    this.poiY = poiY;
  }

  public Short getStatus() {
    return status == null ? CONTACT_STATUS.CONTACT_STATUS_VALID : status;
  }

  public void setStatus(Short status) {
    Preconditions.checkNotNull(status, "status != null");
    Preconditions.checkArgument(status == CONTACT_STATUS.CONTACT_STATUS_EXPIRED
        || status == CONTACT_STATUS.CONTACT_STATUS_VALID, "status not in (1,2)");
    this.status = status;
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  public Date getGmtModified() {
    return gmtModified;
  }

  public void setGmtModified(Date gmtModified) {
    this.gmtModified = gmtModified;
  }

  public static class CONTACT_STATUS implements Serializable {

    private static final long serialVersionUID = -3467189058053114473L;

    /**
     * 联系人过期
     */
    public static final Short CONTACT_STATUS_EXPIRED = 2;

    /**
     * 联系人正常
     */
    public static final Short CONTACT_STATUS_VALID = 1;
  }
}
