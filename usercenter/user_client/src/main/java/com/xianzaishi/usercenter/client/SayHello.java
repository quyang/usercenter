package com.xianzaishi.usercenter.client;

/**
 * demo interface
 * @author zhancang
 */
public interface SayHello {
  public String sayHello(String name);
}
